class AddAttachmentToArchives < ActiveRecord::Migration
  def change
    add_column :archives, :attachment, :string
  end
end
