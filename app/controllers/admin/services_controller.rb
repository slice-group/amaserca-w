module Admin
  # ServicesController
  class ServicesController < AdminController
    before_action :set_service, only: [:show, :edit, :update, :destroy]
    before_action :show_history, only: [:index]

    # GET /services
    def index
      services = Service.searching(@query).all
      @objects = services.page(@current_page)
      @total = services.size
      if !@objects.first_page? && @objects.size.zero?
        redirect_to services_path(page: @current_page.to_i.pred, search: @query)
      end
    end

    # GET /services/1
    def show
    end

    # GET /services/new
    def new
      @service = Service.new
    end

    # GET /services/1/edit
    def edit
    end

    # POST /services
    def create
      @service = Service.new(service_params)

      if @service.save
        redirect(@service, params)
      else
        render :new
      end
    end

    # PATCH/PUT /services/1
    def update
      if @service.update(service_params)
        redirect(@service, params)
      else
        render :edit
      end
    end

    # DELETE /services/1
    def destroy
      @service.destroy
      redirect_to admin_services_path, notice: actions_messages(@service)
    end

    def destroy_multiple
      Service.destroy redefine_ids(params[:multiple_ids])
      redirect_to(
        admin_services_path(page: @current_page, search: @query),
        notice: actions_messages(Service.new)
      )
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_service
      @service = Service.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def service_params
      params.require(:service).permit(:title, :body, :modal_body)
    end

    def show_history
      get_history(Service)
    end
  end
end
