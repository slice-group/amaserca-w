module Admin
  # ArchivesController
  class ArchivesController < AdminController
    before_action :set_archive, only: [:show, :edit, :update, :destroy]
    before_action :show_history, only: [:index]

    # GET /archives
    def index
      archives = Archive.searching(@query).all
      @objects = archives.page(@current_page)
      @total = archives.size
      if !@objects.first_page? && @objects.size.zero?
        redirect_to archives_path(page: @current_page.to_i.pred, search: @query)
      end
    end

    # GET /archives/1
    def show
    end

    # GET /archives/new
    def new
      @archive = Archive.new
    end

    # GET /archives/1/edit
    def edit
    end

    # POST /archives
    def create
      @archive = Archive.new(archive_params)

      if @archive.save
        redirect(@archive, params)
      else
        render :new
      end
    end

    # PATCH/PUT /archives/1
    def update
      if @archive.update(archive_params)
        redirect(@archive, params)
      else
        render :edit
      end
    end

    # DELETE /archives/1
    def destroy
      @archive.destroy
      redirect_to admin_archives_path, notice: actions_messages(@archive)
    end

    def destroy_multiple
      Archive.destroy redefine_ids(params[:multiple_ids])
      redirect_to(
        admin_archives_path(page: @current_page, search: @query),
        notice: actions_messages(Archive.new)
      )
    end

    private

    # Use callbacks to share common setup or constraints between actions.
    def set_archive
      @archive = Archive.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def archive_params
      params.require(:archive).permit(:title, :description, :attachment)
    end

    def show_history
      get_history(Archive)
    end
  end
end
