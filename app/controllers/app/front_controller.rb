module App
  # FrontController
  class FrontController < AppController
    def index
      @message = KepplerContactUs::Message.new
      @services = Service.all
      @attachments = Archive.all
      @attachment_desktop = Archive.all.each_slice(4).to_a
    end
  end
end
