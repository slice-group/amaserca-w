$(document).on('ready page:load', function() {
  var scroll;
  if (window.location.hash !== "") {
    scroll = parseInt($("#" + window.location.hash.substring(1)).offset().top - 30);
    window.scrollTo(0, scroll);
  }
  return $(window).bind("scroll", function() {
    var num;
    num = parseInt($("#home").css("height"));
    if ($(window).scrollTop() >= num - (num * 0.70)) {
      $(".navbar-nav").css({
        "padding-top": "20px",
        "transition": "all 0.3s ease"
      });
      $(".app-navbar").css({
        "box-shadow": "0 3px rgba(0, 0, 0, 0.1)",
        "transition": "all 0.3s ease"
      });
      $("#brand-img").css({
        "width": "70%",
        "transition": "all 0.3s ease"
      });
    } else {
      $(".navbar-nav").css({
        "padding-top": "40px",
        "transition": "all 0.3s ease"
      });
      $(".app-navbar").css({
        "box-shadow": "none",
        "transition": "all 0.3s ease"
      });
      $("#brand-img").css({
        "width": "100%",
        "transition": "all 0.3s ease"
      });
    }
  });
});