# Archive Model
class Archive < ActiveRecord::Base
  include ElasticSearchable
  include ActivityHistory
  mount_uploader :attachment, AttachmentUploader

  def self.query(query)
    { query: { multi_match: {
      query: query,
      fields: [:title, :description],
      operator: :and,
      lenient: true }
    }, sort: { id: 'desc' }, size: count }
  end

  # Build index elasticsearch
  def as_indexed_json(_options = {})
    as_json(
      only: [:id, :title, :description]
    )
  end
end
