# Agregar datos de configuración
KepplerContactUs.setup do |config|
	config.mailer_to = "info@amarserca.com"
	config.mailer_from = "info@amarserca.com"
	config.name_web = "AMARSER CA"
	#Route redirection after send
	config.redirection = "/"


	# Agregar keys de google recaptcha
	Recaptcha.configure do |config|
	  config.public_key  = "6LfydQcUAAAAAKIodzTS7-oe-wqnUlnWS_1ON4OF"
	  config.private_key = "6LfydQcUAAAAACiVuD69OQxwTQuq__gTYYXNNtVk"
	end
end